using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using VUPayRoll.Database;
using VUPayRoll.Entities;
using VUPayRoll.Repository;
using VUPayRoll.Service;

namespace VUPayRoll.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(option1 =>
            {
                option1.AddPolicy("AllowOrigin", option => option.AllowAnyOrigin());
            });

            services.AddDbContextPool<VUPayRollDBContext>(options => options.UseSqlServer(Configuration.GetConnectionString("VUPayRollDBConnection")));
            services.AddControllers();
            services.AddMvc().AddXmlSerializerFormatters();
            var config = new AutoMapper.MapperConfiguration(c =>
            {
                c.AddProfile(new AutoMapperProfile());
            });
            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);

            services.AddScoped< IVUPayRoll<EmployeeInfo>, RVUPayRoll<EmployeeInfo>>();
            services.AddScoped< IVUPayRoll<Religion>, RVUPayRoll<Religion>>();
            services.AddScoped<EmployeeInfoServices>();
            services.AddScoped<ReligionService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();
            app.UseCors(options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }


}
