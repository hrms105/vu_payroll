﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VUPayRoll.Entities;
using VUPayRoll.Repository;

namespace VUPayRoll.API.Controllers
{
    [Route("EmployeeAllownces")]
    //[Route("api/[controller]")]
    [ApiController]
    public class EmployeeAllownceController : ControllerBase 
    {
        private readonly IVUPayRoll<EmployeeAllownce> I_EmployeeAllownce;
        public EmployeeAllownceController(IVUPayRoll<EmployeeAllownce> employeeAllownce)
        {
            I_EmployeeAllownce = employeeAllownce;
        }

        [HttpGet]
        public async Task<IEnumerable<EmployeeAllownce>> GetAllAsync()
        {
            var empAllownce = await I_EmployeeAllownce.GetAll();
            return empAllownce;
        }

        //GET: api/EmployeeInfo/5
        [HttpGet("{id}", Name = "GetAllownce")]
        public async Task<EmployeeAllownce> GetAsync(int id)
        {
            var empAllownce = await I_EmployeeAllownce.Get(1);
            return empAllownce;
        }
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}