﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VUPayRoll.Repository;
using VUPayRoll.Entities;
using VUPayRoll.Service;
using VUPayRoll.ViewModel;

namespace VUPayRoll.API.Controllers
{
    [Route("EmployeeInfo")]
    //[Route("api/[controller]")]
    [ApiController]
    public class EmployeeInfoController : ControllerBase
    {
       // private readonly IVUPayRoll<EmployeeInfo> I_EmployeeInfo;
        private readonly EmployeeInfoServices _service;
        public EmployeeInfoController(/*IVUPayRoll<EmployeeInfo> employeeInfo*/ EmployeeInfoServices service)
        {
            //I_EmployeeInfo = employeeInfo;
            _service = service;
        }

        [HttpGet("getEmp")]

        public async Task<IActionResult> GetAllAsync()
        {
            var employees = await _service.GetAll();
            return Ok(employees);
        }

        //GET: api/EmployeeInfo/5
        [HttpGet("{id}", Name = "GetEmp")]
        public async Task<IActionResult> GetAsync(int id)
        {
            var employees = await _service.Get(id);
            return Ok(employees);
        }

        [HttpPost("AddEmployees")]
        public async Task<IActionResult> AddAsync([FromBody] EmployeeInfoViewModel employeeInfoViewModel)
        {
            var employees = await _service.Add(employeeInfoViewModel);
            return Ok(employees);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateAsync(EmployeeInfoViewModel employeeInfoViewModel)
        {
            var employes = await _service.Update(employeeInfoViewModel);
            return Ok(employes);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var employees = await _service.Delete(id);
            return Ok(employees);
        }
    }
}