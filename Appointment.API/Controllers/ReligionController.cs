﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VUPayRoll.Service;
using VUPayRoll.ViewModel;

namespace VUPayRoll.API.Controllers
{
    [Route("Religion")]
    //[Route("api/[controller]")]
    [ApiController]
    public class ReligionController : ControllerBase
    {
        private readonly ReligionService _service;
        public ReligionController(/*IVUPayRoll<EmployeeInfo> employeeInfo*/ ReligionService service)
        {
            //I_EmployeeInfo = employeeInfo;
            _service = service;
        }

        [HttpGet("getReligion")]
        public async Task<IActionResult> GetAllAsync()
        {
            var employees = await _service.GetAll();
            return Ok(employees);
        }

        //GET: api/EmployeeInfo/5
        //[HttpGet("{id}", Name = "GetEmp")]
        //public async Task<IActionResult> GetAsync(int id)
        //{
        //    var employees = await _service.Get(id);
        //    return Ok(employees);
        //}

        [HttpPost("AddReligion")]
        public async Task<IActionResult> AddAsync([FromBody] ReligionViewModel religionViewModel)
        {
            var religion = await _service.Add(religionViewModel);
            return Ok(religion);
        }

        //[HttpPost]
        //public async Task<IActionResult> UpdateAsync(EmployeeInfoViewModel employeeInfoViewModel)
        //{
        //    var employes = await _service.Update(employeeInfoViewModel);
        //    return Ok(employes);
        //}

        //// DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteAsync(int id)
        //{
        //    var employees = await _service.Delete(id);
        //    return Ok(employees);
        //}
    }
}