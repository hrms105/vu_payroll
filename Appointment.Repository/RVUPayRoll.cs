﻿using VUPayRoll.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VUPayRoll.Database;

namespace VUPayRoll.Repository
{
    public class RVUPayRoll<TEntity> : IVUPayRoll<TEntity> where TEntity: class
    {
        
        private readonly VUPayRollDBContext _context = null;
       // private DbSet<TEntity> table = null;

        public RVUPayRoll(VUPayRollDBContext context)
        {
            this._context = context;
           // table = context.Set<TEntity>();
        }

        //EmployeeInfo Interface Implementation

        public async Task<TEntity> Get(int Id)
        {
            return await _context.Set<TEntity>().FindAsync(Id);
        }

        public async Task<IEnumerable<TEntity>> GetAll()
        {
            return await _context.Set<TEntity>().ToListAsync();
        }

        public async Task<TEntity> Add(TEntity t) 
        {
            await _context.Set<TEntity>().AddAsync(t);
            await _context.SaveChangesAsync();
            return t;
        }

        public async Task<TEntity> Update(TEntity tUpdation)
        {
            var customer = _context.Set<TEntity>().Attach(tUpdation);
            customer.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            await _context.SaveChangesAsync();
            return tUpdation;
        }

        public async Task<TEntity> Delete(int id)
        {
            
            TEntity entity = await _context.Set<TEntity>().FindAsync(id);
            if (entity != null)
            {
                _context.Set<TEntity>().Remove(entity);
                await _context.SaveChangesAsync();
            }
            return entity;
        }


        ////EmployeeAllownce Interface Implementation

        //public async Task<EmployeeAllownce> GetEmpAllownce(int Id)
        //{
        //    return await context.EmployeeAllownces.FindAsync(Id);
        //}

        //public async Task<IEnumerable<EmployeeAllownce>> GetAllEmpAllownce()
        //{
        //    return await context.EmployeeAllownces.ToListAsync();
        //}

        //public async Task<EmployeeAllownce> AddEmpAllownce(EmployeeAllownce employeeAllownce)
        //{
        //    await context.EmployeeAllownces.AddAsync(employeeAllownce);
        //    await context.SaveChangesAsync();
        //    return employeeAllownce;
        //}

        //public async Task<EmployeeAllownce> UpdateEmpAllownce(EmployeeAllownce AllownceUpdation)
        //{
        //    var empAllownce = context.EmployeeAllownces.Attach(AllownceUpdated);
        //    empAllownce.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        //    await context.SaveChangesAsync();
        //    return AllownceUpdated;
        //}

        //public async Task<EmployeeAllownce> DeleteEmpAllownce(int id)
        //{

        //    EmployeeAllownce employeeAllownce = await context.EmployeeAllownces.FindAsync(id);
        //    if (employeeAllownce != null)
        //    {
        //        context.EmployeeAllownces.Remove(employeeAllownce);
        //        await context.SaveChangesAsync();
        //    }
        //    return employeeAllownce;
        //}

        ////Allownce Interface Implementation

        //public async Task<Allownce> GetAllownce(int Id)
        //{
        //    return await context.Allownces.FindAsync(Id);
        //}

        //public async Task<IEnumerable<Allownce>> GetAllAllownce()
        //{
        //    return await context.Allownces.ToListAsync();
        //}

        //public async Task<Allownce> AddAllownce(Allownce allownce)
        //{
        //    await context.Allownces.AddAsync(allownce);
        //    await context.SaveChangesAsync();
        //    return allownce;
        //}

        //public async Task<Allownce> UpdateAllownce(Allownce allownceUpdation)
        //{
        //    var allownce = context.Allownces.Attach(allownceUpdation);
        //    allownce.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        //    await context.SaveChangesAsync();
        //    return allownceUpdation;
        //}

        //public async Task<Allownce> DeleteAllownce(int id)
        //{

        //    Allownce allownce = await context.Allownces.FindAsync(id);
        //    if (allownce != null)
        //    {
        //        context.Allownces.Remove(allownce);
        //        await context.SaveChangesAsync();
        //    }
        //    return allownce;
        //}


        ////Dependent Interface Implementation

        //public async Task<Dependent> GetDependent(int Id)
        //{
        //    return await context.Dependents.FindAsync(Id);
        //}

        //public async Task<IEnumerable<Dependent>> GetAllDependent()
        //{
        //    return await context.Dependents.ToListAsync();
        //}

        //public async Task<Dependent> AddDependent(Dependent dependent)
        //{
        //    await context.Dependents.AddAsync(dependent);
        //    await context.SaveChangesAsync();
        //    return dependent;
        //}

        //public async Task<Dependent> UpdateDependent(Dependent DependentUpdation)
        //{
        //    var dependent= context.Dependents.Attach(DependentUpdated);
        //    dependent.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        //    await context.SaveChangesAsync();
        //    return DependentUpdated;
        //}

        //public async Task<Dependent> DeleteDependent(int id)
        //{

        //    Dependent dependent = await context.Dependents.FindAsync(id);
        //    if (dependent != null)
        //    {
        //        context.Dependents.Remove(dependent);
        //        await context.SaveChangesAsync();
        //    }
        //    return dependent;
        //}

        ////Document Interface Implementation
        //public async Task<Document> GetDocument(int Id)
        //{
        //    return await context.Documents.FindAsync(Id);
        //}

        //public async Task<IEnumerable<Document>> GetAllDocument()
        //{
        //    return await context.Documents.ToListAsync();
        //}

        //public async Task<Document> AddDocument(Document document)
        //{
        //    await context.Documents.AddAsync(document);
        //    await context.SaveChangesAsync();
        //    return document;
        //}

        //public async Task<Document> UpdateDocument(Document DocumentUpdation)
        //{
        //    var document = context.Documents.Attach(DocumentUpdated);
        //    document.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        //    await context.SaveChangesAsync();
        //    return DocumentUpdated;
        //}

        //public async Task<Document> DeleteDocument(int id)
        //{

        //    Document document = await context.Documents.FindAsync(id);
        //    if (document != null)
        //    {
        //        context.Documents.Remove(document);
        //        await context.SaveChangesAsync();
        //    }
        //    return document;
        //}


        ////VacationAndSick Interface Implementation
        //public async Task<VacationAndSick> GetVacationAndSick(int Id)
        //{
        //    return await context.VacationAndSicks.FindAsync(Id);
        //}

        //public async Task<IEnumerable<VacationAndSick>> GetAllVacationAndSick()
        //{
        //    return await context.VacationAndSicks.ToListAsync();
        //}

        //public async Task<VacationAndSick> AddVacationAndSick(VacationAndSick vacation)
        //{
        //    await context.VacationAndSicks.AddAsync(vacation);
        //    await context.SaveChangesAsync();
        //    return vacation;
        //}

        //public async Task<VacationAndSick> UpdateVacationAndSick(VacationAndSick vacationUpdation)
        //{
        //    var document = context.VacationAndSicks.Attach(vacationUpdation);
        //    document.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        //    await context.SaveChangesAsync();
        //    return vacationUpdation;
        //}

        //public async Task<VacationAndSick> DeleteVacationAndSick(int id)
        //{

        //    VacationAndSick vacation = await context.VacationAndSicks.FindAsync(id);
        //    if (vacation != null)
        //    {
        //        context.VacationAndSicks.Remove(vacation);
        //        await context.SaveChangesAsync();
        //    }
        //    return vacation;
        //}


        ////DesignationType Interface Implementation
        //public async Task<DesignationType> GetDesignationType(int Id)
        //{
        //    return await context.DesignationTypes.FindAsync(Id);
        //}

        //public async Task<IEnumerable<DesignationType>> GetAllDesignationType()
        //{
        //    return await context.DesignationTypes.ToListAsync();
        //}

        //public async Task<DesignationType> AddDesignationType(DesignationType designationType)
        //{
        //    await context.DesignationTypes.AddAsync(designationType);
        //    await context.SaveChangesAsync();
        //    return designationType;
        //}

        //public async Task<DesignationType> UpdateDesignationType(DesignationType designationTypeUpdation)
        //{
        //    var designation = context.DesignationTypes.Attach(designationTypeUpdation);
        //    designation.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        //    await context.SaveChangesAsync();
        //    return designationTypeUpdation;
        //}

        //public async Task<DesignationType> DeleteDesignationType(int id)
        //{

        //    DesignationType designation = await context.DesignationTypes.FindAsync(id);
        //    if (designation != null)
        //    {
        //        context.DesignationTypes.Remove(designation);
        //        await context.SaveChangesAsync();
        //    }
        //    return designation;
        //}
    }
}
