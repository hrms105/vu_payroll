﻿using VUPayRoll.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace VUPayRoll.Database
{
    public class VUPayRollDBContext : DbContext
    {
        public VUPayRollDBContext(DbContextOptions<VUPayRollDBContext> options) 
            : base(options)
        {

        }
        public DbSet<EmployeeInfo> Employees { get; set; }
        public DbSet<EmployeeAllownce> EmployeeAllownces { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<DocumentType> DocumentTypes { get; set; }
        public DbSet<Dependent> Dependents { get; set; }
        public DbSet<VacationAndSick> VacationAndSicks { get; set; }
        public DbSet<RelationshipType> RelationshipTypes { get; set; }
        public DbSet<LeaveType> LeaveTypes { get; set; }
        public DbSet<DesignationType> DesignationTypes { get; set; }
        public DbSet<Religion> Religions { get; set; }
        public DbSet<Allownce> Allownces { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
    }
}
