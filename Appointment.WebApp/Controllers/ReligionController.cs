﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using VUPayRoll.ViewModel;
using VUPayRoll.WebApp;

namespace Appointment.WebApp.Controllers
{
    public class ReligionController : Controller
    {
        // GET: Religion
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async  Task<ActionResult> Create(ReligionViewModel religionViewModel)
        {
            using (var client = Utility.GetHttpClient())
            {
                //var Client = new HttpClient();
                var myContent = JsonConvert.SerializeObject(religionViewModel);
                var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage response = await client.PostAsync("Religion/AddReligion", byteContent);
            }
            return View();
        }
    }
}