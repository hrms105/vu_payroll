﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using VUPayRoll.Entities;
using VUPayRoll.ViewModel;

namespace VUPayRoll.WebApp.Controllers
{
    public class EmployeeInfoController : Controller
    {

        //public ActionResult GetEmployees()
        //{
        //    IEnumerable<EmployeeInfo> emp = null;

        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri("https://localhost:44332/");

        //        //Called Member default GET All records  
        //        //GetAsync to send a GET request   
        //        // PutAsync to send a PUT request  
        //        var responseTask = client.GetAsync("");
        //        responseTask.Wait();

        //        //To store result of web api response.   
        //        var result = responseTask.Result;

        //        //If success received   
        //        if (result.IsSuccessStatusCode)
        //        {
        //            var readTask = result.Content.ReadAsAsync<IList<EmployeeInfo>>();
        //            readTask.Wait();

        //            emp = readTask.Result;
        //        }
        //        else
        //        {
        //            //Error response received   
        //            emp = Enumerable.Empty<EmployeeInfo>();
        //            ModelState.AddModelError(string.Empty, "Server error try after some time.");
        //        }
        //    }
        //    return View(emp);
       // }
        // GET: EmployeeInfo
        [HttpGet]
        public ActionResult create()
        {
            return View();
        }
        // POST: EmployeeInfo
        [HttpPost]
        public ActionResult create(EmployeeInfo employeeInfo)
        {

            return RedirectToAction("Employment", employeeInfo);
            // return View("Employment", employeeInfo);
        }
        [HttpGet]
        public ActionResult Employment(EmployeeInfo employeeInfo)
        {
            return View(employeeInfo);
        }
        [HttpPost]
        public async Task<ActionResult> Employment(EmployeeInfoViewModel employeeInfoViewModel)
        {
           // IEnumerable<EmployeeInfo> emp = null;
            using (var client = Utility.GetHttpClient())
            {
                //var Client = new HttpClient();
                var myContent = JsonConvert.SerializeObject(employeeInfoViewModel);
                var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage response = await client.PostAsync("EmployeeInfo/AddEmployees", byteContent);
            }
            return View();



                // GET: EmployeeInfo/Details/5
                //public ActionResult Details(int id)
                //{
                //    return View();
                //}

                //[HttpPost]
                // GET: EmployeeInfo/Create
                //public ActionResult Create()
                //{
                //    return View();
                //}

                // POST: EmployeeInfo/Create
                //[HttpPost]
                //public ActionResult Create(FormCollection collection)
                //{
                //    try
                //    {
                //        //using(var emoloyee = new VUPayRollDBContext(VUPayRollDBConnection))
                //        // TODO: Add insert logic here

                //        return RedirectToAction("Index");
                //    }
                //    catch
                //    {
                //        return View();
                //    }
                //}

                // GET: EmployeeInfo/Edit/5
                //public ActionResult Edit(int id)
                //{
                //    return View();
                //}

                //// POST: EmployeeInfo/Edit/5
                //[HttpPost]
                //public ActionResult Edit(int id, FormCollection collection)
                //{
                //    try
                //    {
                //        // TODO: Add update logic here

                //        return RedirectToAction("Index");
                //    }
                //    catch
                //    {
                //        return View();
                //    }
                //}

                //// GET: EmployeeInfo/Delete/5
                //public ActionResult Delete(int id)
                //{
                //    return View();
                //}

                //// POST: EmployeeInfo/Delete/5
                //[HttpPost]
                //public ActionResult Delete(int id, FormCollection collection)
                //{
                //    try
                //    {
                //        // TODO: Add delete logic here

                //        return RedirectToAction("Index");
                //    }
                //    catch
                //    {
                //        return View();
                //    }
                //}
            }
        }
}
