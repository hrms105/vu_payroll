﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VUPayRoll.Entities
{
    public class EmployeeInfo : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CNIC { get; set; }
        public DateTime DOB { get; set; }
        public GenderEnum Gender { get; set; }
        public MaritalStatusEnum MaritalStatus { get; set; }
        public int ReligionId { get; set; }
        public virtual Religion religion { get; set; }
        public int CountryId { get; set; }
        public virtual Country country { get; set; }
        public int CityId { get; set; }
        public virtual City city { get; set; }
        public string Nationality { get; set; }
        public string StreetAddress { get; set; }
        public string ZipCode { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyContactNo { get; set; }
        public string Mobile { get; set; }
        public DateTime HireDate { get; set; }
        public DateTime JoiningDate { get; set; }
        public string Status { get; set; }
        public string EmployeeType { get; set; }
        public DateTime PermanentDate { get; set; }
        public DateTime SettlementDate { get; set; }
        public string Location { get; set; }
        public ShiftEnum Shift { get; set; }
        public int DesignationTypeId { get; set; }
        public virtual DesignationType designationType { get; set; }
        public string Speciality { get; set; }
        public string Classification { get; set; }
        public string Department { get; set; }
        public string NationalId { get; set; }
        public string PassportNo { get; set; }
        public string EmployeeCode { get; set; }
        public bool MedicalInsurance { get; set; }
        public bool AirTicket { get; set; }
        public PayTypeEnum PayType { get; set; }
        public decimal BasicSalary { get; set; }
        public SalaryPaymentMethodEnum SalaryPaymentMethod { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string BranchCode { get; set; }
        public string AccountNo { get; set; }
        public string SwiftCode { get; set; }

    }
}
