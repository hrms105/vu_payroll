﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VUPayRoll.Entities
{
    public class Document : BaseEntity
    {
        public int DocumentTypeId { get; set; }
        public virtual DocumentType documentType { get; set; }
        public int EmployeeInfoId { get; set; }
        public virtual EmployeeInfo emloyeeInfo { get; set; }
        public string Description { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string SelectFile { get; set; }
    }
}
