﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VUPayRoll.Entities
{
    public class RelationshipType : BaseEntity
    {
        public string Name { get; set; }
    }
}
