﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VUPayRoll.Entities
{
    public class Dependent : BaseEntity
    {
        public int RelationshipTypeId { get; set; }
        public virtual RelationshipType relationshipType { get; set; }
        public int EmployeeInfoId { get; set; }
        public virtual EmployeeInfo employeeInfo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool EmergencyContact { get; set; }
        public bool TicketEligible { get; set; }
        public string CNIC { get; set; }
        public DateTime DOB { get; set; }
        public GenderEnum Gender { get; set; }
        public MaritalStatusEnum MaritalStatus { get; set; }
        public string PassportNo { get; set; }
        //public int ReligionId { get; set; }
        //public Religion religion { get; set; }
        public string Nationality { get; set; }
        public string Mobile { get; set; }
        public string Remarks { get; set; }
    }
}
