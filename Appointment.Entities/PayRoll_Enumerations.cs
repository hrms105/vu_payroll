﻿namespace VUPayRoll.Entities
{
    public enum GenderEnum
    {
        Male,
        Female,
        Others
    }
    public enum ShiftEnum
    {
        Morning,
        Evening,
        Night
    }
    public enum MaritalStatusEnum
    {
        Single,
        Married,
        Divorced
    }
    public enum PayTypeEnum
    {
        Weekly,
        Monthly,
        Early
    }
    public enum SalaryPaymentMethodEnum
    {
        Bank_Transfer,
        Cash
    }
}
