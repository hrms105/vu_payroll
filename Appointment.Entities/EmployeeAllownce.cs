﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VUPayRoll.Entities
{
    public class EmployeeAllownce : BaseEntity
    {
        public int AllownceId { get; set; }
        public virtual Allownce allownce { get; set; }
        public int EmployeeInfoId { get; set; }
        public virtual EmployeeInfo employeeInfo { get; set; }
        public decimal Amount { get; set; }
    }
}
