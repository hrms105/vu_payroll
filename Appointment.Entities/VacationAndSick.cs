﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VUPayRoll.Entities
{
    public class VacationAndSick : BaseEntity
    {
        public int LeaveTypeId { get; set; }
        public virtual LeaveType leaveType { get; set; }
        public int EmployeeInfoId { get; set; }
        public virtual EmployeeInfo employeeInfo { get; set; }
        public int Days { get; set; }
    }
}
