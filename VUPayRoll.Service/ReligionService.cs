﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VUPayRoll.Entities;
using VUPayRoll.Repository;
using VUPayRoll.ViewModel;

namespace VUPayRoll.Service
{
    public class ReligionService
    {
        private readonly IVUPayRoll<Religion> _repo;
        private readonly IMapper _mapper;

        public ReligionService(IVUPayRoll<Religion> repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ReligionViewModel>> GetAll()
        {
            var data = await _repo.GetAll();
            var religion = _mapper.Map<IEnumerable<ReligionViewModel>>(data);
            return religion;
        }

        public async Task<ReligionViewModel> Get(int id)
        {
            var religion = await _repo.Get(id);
            var religionVM = _mapper.Map<ReligionViewModel>(religion);
            return religionVM;
        }

        public async Task<Religion> Add(ReligionViewModel religionViewModel)
        {
            var religion = _mapper.Map<Religion>(religionViewModel);
            var religionVM = await _repo.Add(religion);
            return religionVM;
        }

        //public async Task<EmployeeInfo> Update(EmployeeInfoViewModel employeeInfoView)
        //{
        //    var userM = _mapper.Map<EmployeeInfo>(employeeInfoView);
        //    var employeeInfo = await _repo.Update(userM);
        //    return employeeInfo;
        //}

        //public async Task<EmployeeInfo> Delete(int id)
        //{
        //    var employeeInfo = await _repo.Delete(id);
        //    return employeeInfo;
        //}

    }
}
