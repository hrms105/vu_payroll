﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using VUPayRoll.Entities;
using VUPayRoll.ViewModel;

namespace VUPayRoll.Service
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<EmployeeInfoViewModel, EmployeeInfo>();
            CreateMap<ReligionViewModel, Religion>();
        }
    }
}
