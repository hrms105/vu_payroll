﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VUPayRoll.Entities;
using VUPayRoll.Repository;
using VUPayRoll.ViewModel;

namespace VUPayRoll.Service
{
    public class EmployeeInfoServices
    {

        private readonly IVUPayRoll<EmployeeInfo> _repo;
        private readonly IMapper _mapper;

        public EmployeeInfoServices(IVUPayRoll<EmployeeInfo> repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        public async Task<IEnumerable<EmployeeInfoViewModel>> GetAll()
        {
            var data = await _repo.GetAll();
            var employee = _mapper.Map<IEnumerable<EmployeeInfoViewModel>>(data);
            return employee;
        }

        public async Task<EmployeeInfoViewModel> Get(int id)
        {
            var employee = await _repo.Get(id);
            var employeeVM = _mapper.Map<EmployeeInfoViewModel>(employee);
            return employeeVM;
        }

        public async Task<EmployeeInfo> Add(EmployeeInfoViewModel employeeInfoViewModel)
        {
            var userM = _mapper.Map<EmployeeInfo>(employeeInfoViewModel);
            var employeeInfo = await _repo.Add(userM);
            return employeeInfo;
        }

        public async Task<EmployeeInfo> Update(EmployeeInfoViewModel employeeInfoView)
        {
            var userM = _mapper.Map<EmployeeInfo>(employeeInfoView);
            var employeeInfo= await _repo.Update(userM);
            return employeeInfo;
        }

        public async Task<EmployeeInfo> Delete(int id)
        {
            var employeeInfo = await _repo.Delete(id);
            return employeeInfo;
        }

    }
}
